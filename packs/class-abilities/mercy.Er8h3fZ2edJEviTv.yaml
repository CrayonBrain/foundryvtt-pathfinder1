_id: Er8h3fZ2edJEviTv
_key: '!items!Er8h3fZ2edJEviTv'
img: systems/pf1/icons/feats/toughness.jpg
name: Mercy
system:
  abilityType: su
  actions:
    - _id: lgerocqoylmcni3p
      activation:
        type: passive
        unchained:
          type: passive
      img: systems/pf1/icons/feats/toughness.jpg
      name: Use
  associations:
    classes:
      - Paladin
  description:
    value: >-
      <p>At 3rd level, and every three levels thereafter, a paladin can select
      one mercy. Each mercy adds an effect to the paladin’s lay on hands
      ability. Whenever the paladin uses lay on hands to heal damage to one
      target, the target also receives the additional effects from all of the
      mercies possessed by the paladin. A mercy can remove a condition caused by
      a curse, disease, or poison without curing the affliction. Such conditions
      return after 1 hour unless the mercy actually removes the affliction that
      causes the condition.</p>

      <p>At 3rd level, the paladin can select from the following initial
      mercies.</p>

      <ul>

      <li><em>Deceived:</em> The target can immediately attempt a new saving
      throw to disbelieve any ongoing illusions that it failed to disbelieve
      within the last minute.</li>

      <li><em>Fatigued: </em>The target is no longer <em>fatigued</em>.</li>

      <li><em>Riled:</em> The <em>paladin’s lay on hands</em> also acts as
      <em>calm emotions</em>, but only for the purpose of suppressing morale
      bonuses (such as from the <em>rage</em> spell) and emotion effects that
      aren’t fear effects. Use the <em>paladin’s</em> level as the <em>caster
      level</em>. </li>

      <li><em>Shaken:</em> The target is no longer <em>shaken</em>.</li>

      <li><em>Sickened:</em> The target is no longer <em>sickened</em>.</li>

      </ul>

      <p>At 6th level, a paladin adds the following mercies to the list of those
      that can be selected.</p>

      <ul>

      <li><em>Dazed:</em> The target is no longer <em>dazed</em>.</li>

      <li><em>Diseased:</em> The paladin’s lay on hands ability also acts as
      <em>remove disease</em>, using the paladin’s level as the caster
      level.</li>

      <li><em>Enfeebled:</em> The <em>paladin</em> dispels any magical effects
      that are reducing one of the target’s ability scores (<em>paladin’s</em>
      choice). </li>

      <li><em>Haunted:</em> The <em>paladin’s lay on hands</em> also acts as
      <em>protection from evil</em>, but only for the purpose of allowing a new
      saving throw against <em>enchantment</em> (<em>charm</em>) and
      <em>enchantment</em> (<em>compulsion</em>) effects, making the target
      immune to any attempts to possess or exercise mental control over the
      target, or preventing a life force from controlling the target (all as
      described in the second effect of <em>protection from evil</em>). Use the
      <em>paladin’s</em> level as the <em>caster level</em>. </li>

      <li><em>Staggered: </em>The target is no longer <em>staggered</em>, unless
      the target is at exactly 0 hit points.</li>

      <li><em>Targeted: </em>The <em>paladin’s lay on hands</em> also acts as
      <em>sanctuary</em>, using the <em>paladin’s</em> level as the <em>caster
      level</em>. The saving throw DC to negate this effect is equal to 10 + 1/2
      the <em>paladin’s</em> level + the <em>paladin’s Charisma</em> modifier.
      </li>

      </ul>

      <p>At 9th level, a paladin adds the following mercies to the list of those
      that can be selected.</p>

      <ul>

      <li><em>Confused: </em>The target is no longer <em>confused</em>.</li>

      <li><em>Cursed:</em> The paladin’s lay on hands ability also acts as
      <em>remove curse</em>, using the paladin’s level as the caster level.</li>

      <li><em>Exhausted: </em>The target is no longer <em>exhausted</em>. The
      paladin must have the fatigue mercy before selecting this mercy.</li>

      <li><em>Frightened: </em>The target is no longer <em>frightened</em>. The
      paladin must have the <em>shaken</em> mercy before selecting this
      mercy.</li>

      <li><em>Injured:</em> The target gains <em>fast healing</em> 3 for a
      number of rounds equal to 1/2 the <em>paladin’s</em> level. </li>

      <li><em>Nauseated:</em> The target is no longer <em>nauseated</em>. The
      paladin must have the <em>sickened</em> mercy before selecting this
      mercy.</li>

      <li><em>Poisoned: </em>The paladin’s lay on hands ability also acts as
      <em>neutralize poison</em>, using the paladin’s level as the caster
      level.</li>

      <li><em>Restorative:</em> The target heals 1d4 points of <em>ability
      damage</em> from a single ability score of the <em>paladin’s</em>
      choosing. The <em>paladin</em> must have the enfeebled mercy before
      selecting this mercy. </li>

      </ul>

      <p>At 12th level, a paladin adds the following mercies to the list of
      those that can be selected.</p>

      <ul>

      <li><em>Amputated: </em>The <em>paladin’s lay on hands</em> also acts as
      <em>regenerate</em>, but only for the purposes of regrowing severed body
      members, <em>broken</em> bones, and ruined organs. The <em>paladin</em>
      must have the injured mercy before she can select this mercy. </li>

      <li><em>Blinded:</em> The target is no longer <em>blinded</em>.</li>

      <li><em>Deafened: </em>The target is no longer <em>deafened</em>.</li>

      <li><em>Ensorcelled:</em> The <em>paladin’s lay on hands</em> also acts as
      <em>dispel magic</em>, using the <em>paladin’s</em> level as her
      <em>caster level</em> (maximum 20). Source <em>PPC:HH</em></li>

      <li><em>Paralyzed: </em>The target is no longer <em>paralyzed</em>.</li>

      <li><em>Petrified:</em> The <em>paladin’s lay on hands</em> ability also
      acts as <em>stone to flesh</em>, but only for the purpose of removing the
      <em>petrified</em> condition from a creature. </li>

      <li><em>Stunned:</em> The target is no longer <em>stunned</em>.</li>

      </ul>

      <p>These abilities are cumulative. For example, a 12th-level paladin’s lay
      on hands ability heals 6d6 points of damage and might also cure
      <em>Fatigued</em> and <em>Exhausted</em> conditions as well as removing
      diseases and neutralizing poisons. Once a condition or spell effect is
      chosen, it can’t be changed.</p>
  subType: classFeat
type: feat

