_id: tr7m97npkbgm4wp7
_key: '!items!tr7m97npkbgm4wp7'
img: icons/magic/perception/eye-ringed-glow-angry-small-teal.webp
name: Detect Evil
system:
  actions:
    - _id: mlk2dv2yf5xgq8to
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      area: cone-shaped emanation
      duration:
        units: spec
        value: concentration, up to 10 min./level
      img: systems/pf1/icons/misc/magic-swirl.png
      measureTemplate:
        size: '60'
        type: cone
      name: Use
      range:
        units: ft
        value: '60'
  components:
    divineFocus: 1
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You can sense the presence of evil. The amount of information revealed
      depends on how long you study a particular area or subject.</p><p><i>1st
      Round</i>: Presence or absence of evil.</p><p><i>2nd Round</i>: Number of
      evil auras (creatures, objects, or spells) in the area and the power of
      the most potent evil aura present.</p><p>If you are of good alignment, and
      the strongest evil aura's power is overwhelming (see below), and the HD or
      level of the aura's source is at least twice your character level, you are
      stunned for 1 round and the spell ends.</p><p><i>3rd Round</i>: The power
      and location of each aura. If an aura is outside your line of sight, then
      you discern its direction but not its exact location.</p><p><i>Aura
      Power</i>: An evil aura's power depends on the type of evil creature or
      object that you're detecting and its HD, caster level, or (in the case of
      a cleric) class level; see the table below. If an aura falls into more
      than one strength category, the spell indicates the stronger of the
      two.</p><p><i>Lingering Aura</i>: An evil aura lingers after its original
      source dissipates (in the case of a spell) or is destroyed (in the case of
      a creature or magic item). If <i>detect evil</i> is cast and directed at
      such a location, the spell indicates an aura strength of dim (even weaker
      than a faint aura). How long the aura lingers at this dim level depends on
      its original power: </p><table><tbody><tr><th>Original
      Strength</th><th>Duration of Lingering Aura</th></tr>
      <tr><td>Faint</td><td>1d6 rounds</td></tr> <tr><td>Moderate</td><td>1d6
      minutes</td></tr> <tr><td>Strong</td><td>1d6x10 minutes</td></tr>
      <tr><td>Overwhelming</td><td>1d6 days</td></tr></tbody></table> Animals,
      traps, poisons, and other potential perils are not evil, and as such this
      spell does not detect them. Creatures with actively evil intents count as
      evil creatures for the purpose of this spell.<p></p><p>Each round, you can
      turn to <i>detect evil</i> in a new area. The spell can penetrate
      barriers, but 1 foot of stone, 1 inch of common metal, a thin sheet of
      lead, or 3 feet of wood or dirt blocks it.</p><table><caption>Detect
      Chaos/Evil/Good/Law</caption> <tbody><tr><th
      rowspan="2">Creature/Object</th><th colspan="5">Aura Power</th></tr>
      <tr><th>None</th><th>Faint</th><th>Moderate</th><th>Strong</th><th>Overwhelming</th></tr>
      <tr><td>Aligned creature<sup>1</sup> (HD)</td><td>5 or
      lower</td><td>5-10</td><td>11-25</td><td>26-50</td><td>51 or
      higher</td></tr> <tr><td>Aligned Undead (HD)</td><td>--<br /></td><td>2 or
      lower</td><td>3-8</td><td>9-20</td><td>21 or higher</td></tr>
      <tr><td>Aligned outsider (HD)</td><td>--<br /></td><td>1 or
      lower</td><td>2-4</td><td>5-10</td><td>11 or higher</td></tr>
      <tr><td>Cleric or paladin of an aligned deity<sup>2</sup> (class
      levels)</td><td>--<br /></td><td>1</td><td>2-4</td><td>5-10</td><td>11 or
      higher</td></tr> <tr><td>Aligned magic item or spell (caster
      level)</td><td>5th or
      lower</td><td>6th-10th</td><td>11th-15th</td><td>16th-20th</td><td>21st or
      higher</td></tr> </tbody><tfoot><tr><td colspan="6">1 Except for undead
      and outsiders, which have their own entries on the table.</td></tr>
      <tr><td colspan="6">2 Some characters who are not clerics may radiate an
      aura of equivalent power. The class description will indicate whether this
      applies.</td></tr></tfoot></table>&gt;
  learnedAt:
    class:
      adept: 1
      cleric: 1
      inquisitor: 1
      oracle: 1
      shaman: 1
      spiritualist: 1
      warpriest: 1
  school: div
  sr: false
type: spell

