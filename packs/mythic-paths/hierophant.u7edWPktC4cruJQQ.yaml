_id: u7edWPktC4cruJQQ
_key: '!items!u7edWPktC4cruJQQ'
img: systems/pf1/icons/items/inventory/badge-cross.jpg
name: Hierophant
system:
  bab: ''
  description:
    value: >-
      <p>Those who draw upon a god's power find themselves becoming closer to
      the divine. Some aspire to become immortal servants of their gods, and
      others reach for apotheosis itself. A hierophant could be either of these,
      channeling divine power directly or indirectly — whether from a deity, the
      spirit of nature, or the power of life itself — and eventually becoming
      more like a patron of divine power than a mere devotee.</p><p>Role: As a
      hierophant, your role in the group is to act as a conduit to the divine,
      healing and helping your allies through the miracles that you bestow. This
      focus is just as strong if you are instead a servant of nature, using the
      powers granted to you by this path to protect the wilderness and bring its
      fury to those who would dare to defile it.</p><p>Classes: Members of any
      class that draws upon divine magic will find the path of the hierophant
      useful — particularly clerics, druids, and oracles. Even classes with
      limited divine spellcasting, such as the inquisitor and paladin, have a
      number of useful options in this path.</p><p>Bonus Hit Points: Whenever
      you gain a hierophant tier, you gain 4 bonus hit points. These hit points
      stack with themselves, and don't affect your overall Hit Dice or other
      statistics.</p><h2>Path
      Features</h2><table><tbody><tr><td><p>Tier</p></td><td><p>Path
      Features</p></td></tr><tr><td><p>1st</p></td><td><p>Divine surge, path
      ability</p></td></tr><tr><td><p>2nd</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>3rd</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>4th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>5th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>6th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>7th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>8th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>9th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>10th</p></td><td><p>Divine vessel, path
      ability</p></td></tr></tbody></table><p>As you gain tiers, you gain the
      following abilities.</p><p>Divine Surge: Select one of the following
      abilities. Once chosen, it can't be changed.</p><p>Beast's Fury (Su): As a
      swift action, you can expend one use of mythic power to imbue your animal
      companion, cohort, eidolon, familiar, or bonded mount with some of your
      mythic power. As an immediate action, that creature can move up to its
      speed and make an attack with one of its natural weapons. When making this
      attack, the creature rolls twice and takes the higher result. Any damage
      dealt by this attack bypasses all damage reduction. A creature affected by
      this ability can take these actions in addition to any other actions it
      takes during its turn.</p><p>Inspired Spell (Su): As a standard action,
      you can expend one use of mythic power to cast any one divine spell
      without expending a prepared spell or spell slot. The spell must be on one
      of your divine class spell lists (or your domain or mystery spell list),
      must be of a level that you can cast with that divine spellcasting class,
      and must have a casting time of "1 standard action" (or less). You don't
      need to have the spell prepared, nor does it need to be on your list of
      spells known. When casting a spell in this way, you treat your caster
      level as 2 levels higher for the purpose of any effect dependent on level.
      You can apply any metamagic feats you know to this spell, but its total
      adjusted level can't be greater than that of the highest-level divine
      spell you can cast from that spellcasting class.</p><p>Recalled Blessing
      (Su): You can expend one use of mythic power to cast any one divine spell
      without expending a prepared spell or spell slot. If you prepare spells,
      this spell must be one you prepared today; if you're a spontaneous caster,
      this spell must be one of your spells known. You can't apply metamagic
      feats to this spell. If the spell requires a saving throw, non-mythic
      creatures roll twice and take the lower result. If the spell heals damage
      or requires you to attempt a caster level check to cure an aff liction or
      remove a condition, roll twice and take the higher result.</p><p>Path
      Ability: At 1st tier and every tier thereafter, select one new path
      ability from the hierophant path abilities lists or from the universal
      path abilities lists (see page 50). Once you select an ability, it can't
      be changed. Unless otherwise noted, each ability can be selected only
      once. Some abilities have requirements, such as a class ability or minimum
      mythic tier, that you must meet before you select them.</p><p>Divine
      Vessel (Ex): At 10th tier, whenever you cast a spell that targets one or
      more non-mythic creatures, those creatures must roll twice for any saving
      throws associated with the spell and take the lower result. Whenever you
      are healed of hit point damage by a spell or effect, you are healed for
      the maximum possible amount. You also gain DR 10/epic. Once per round when
      you take more than 20 points of damage (after damage reduction is
      applied), you regain one use of mythic power.</p>
  hd: 4
  hp: 0
  savingThrows:
    fort:
      value: ''
    ref:
      value: ''
    will:
      value: ''
  sources:
    - id: PZO1126
      pages: '32'
  subType: mythic
  tag: mythicHierophant
type: class

