_id: dFfB1SnoXFEqOBwM
_key: '!items!dFfB1SnoXFEqOBwM'
img: systems/pf1/icons/skills/violet_07.jpg
name: Sunbaked Zombie
system:
  changes:
    - _id: r9n473eh
      formula: '2'
      modifier: untyped
      subTarget: str
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Sun-baked zombies most often rise
      near pyramids and other burial sites in hot deserts, where latent
      necromantic energy lingers from countless arcane rituals and restless
      spirits. As such, sunbaked zombies are primarily found among the dunes of
      Osirion and the other nations that make up northern Garund. Typically
      animated in isolation, sunbaked zombies rarely form hordes like normal
      zombies, but when entire caravans fall to thirst and the desert sun, all
      of its members might rise as these terrible undead.<p>When one
      intentionally raises a sunbaked zombie using animate dead, the body to be
      raised must be left out in the sun’s rays for a full 12 hours and must be
      salted every hour during this time to hasten its desiccation. Spell
      effects that produce light work for this purpose only if they count as
      actual sunlight, and even then they must be combined with desecrate.
      Casting the animating spell at night always fails; the sun must be out and
      directly beating down on the corpse. Without the intense magical focus of
      a spell, it takes many days for the corpse to absorb enough sun and
      necromantic energy to rise spontaneously.<p>“Sunbaked zombie” is an
      acquired template that can be added to any corporeal creature (other than
      undead), referred to hereafter as the base creature.<p><b>CR:</b> This
      depends on the creature’s new total number of Hit Dice, as follows.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>HD</b></td>

      <td><b>CR</b></td>

      <td><b>XP</b></td>

      </tr>

      <tr>

      <td>1</td>

      <td>1/2</td>

      <td>200</td>

      </tr>

      <tr>

      <td>2</td>

      <td>1</td>

      <td>400</td>

      </tr>

      <tr>

      <td>3–4</td>

      <td>2</td>

      <td>600</td>

      </tr>

      <tr>

      <td>5–6</td>

      <td>3</td>

      <td>800</td>

      </tr>

      <tr>

      <td>7–8</td>

      <td>4</td>

      <td>1,200</td>

      </tr>

      <tr>

      <td>9–10</td>

      <td>5</td>

      <td>1,600</td>

      </tr>

      <tr>

      <td>11–12</td>

      <td>6</td>

      <td>2,400</td>

      </tr>

      <tr>

      <td>13–16</td>

      <td>7</td>

      <td>3,200</td>

      </tr>

      <tr>

      <td>17–20</td>

      <td>8</td>

      <td>4,800</td>

      </tr>

      <tr>

      <td>21–24</td>

      <td>9</td>

      <td>6,400</td>

      </tr>

      <tr>

      <td>25–28</td>

      <td>10</td>

      <td>9,600</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Alignment:</b> Always neutral evil.<p><b>Type:</b> The
      creature’s type changes to undead. It retains any subtypes except for
      alignment subtypes (such as good) and subtypes that indicate kind (such as
      giant). It does not gain the augmented subtype.<p><b>Armor Class</b>: The
      natural armor bonus is based on the creature’s size.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Sunbaked Zombie Size</b></td>

      <td><b>Natural Armor Bonus</b></td>

      </tr>

      <tr>

      <td>Tiny or smaller</td>

      <td>+0</td>

      </tr>

      <tr>

      <td>Small</td>

      <td>+1</td>

      </tr>

      <tr>

      <td>Medium</td>

      <td>+2</td>

      </tr>

      <tr>

      <td>Large</td>

      <td>+3</td>

      </tr>

      <tr>

      <td>Huge</td>

      <td>+4</td>

      </tr>

      <tr>

      <td>Gargantuan</td>

      <td>+7</td>

      </tr>

      <tr>

      <td>Colossal</td>

      <td>+11</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Hit Dice:</b> Drop Hit Dice gained from class levels (to a
      minimum of 1) and change racial HD to d8s. Sunbaked zombies gain
      additional HD as noted on the following table. Sunbaked zombies use their
      Charisma modifiers to determine bonus hit points (instead of
      Constitution).</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Sunbaked Zombie Size</b></td>

      <td><b>Bonus Hit Dice</b></td>

      </tr>

      <tr>

      <td>Tiny or smaller</td>

      <td>—</td>

      </tr>

      <tr>

      <td>Small or Medium</td>

      <td>+1 HD</td>

      </tr>

      <tr>

      <td>Large</td>

      <td>+2 HD</td>

      </tr>

      <tr>

      <td>Huge</td>

      <td>+4 HD</td>

      </tr>

      <tr>

      <td>Gargantuan</td>

      <td>+6 HD</td>

      </tr>

      <tr>

      <td>Colossal</td>

      <td>+10 HD</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Saves:</b> A sunbaked zombie’s base save bonuses are Fort +1/3
      HD, Ref +1/3 HD, and Will +1/2 HD + 2.<p><b>Defensive Abilities:</b> A
      sunbaked zombie loses the base creature’s defensive abilities and gains DR
      5/slashing and resist fire 10 (or immunity to fire if it has 11 HD or
      more), as well as all of the standard immunities and traits granted by the
      undead type.<p><b>Speed:</b> Winged sunbaked zombies can still fly, but
      their maneuverability drops to clumsy. If the base creature flew
      magically, so can the sunbaked zombie. Retain all other movement
      types.<p><b>Attacks:</b> A sunbaked zombie retains all natural weapons,
      manufactured weapon attacks, and weapon proficiencies of the base
      creature. It also gains a slam attack that deals damage based on the
      sunbaked zombie’s size, but as if it were one size category larger than
      its actual size (Pathfinder RPG Bestiary 301–302).<p><b>Special
      Attacks:</b> A sunbaked zombie retains none of the base creature’s special
      attacks, but gains the following.<p><em>Death Throes (Su): </em>When a
      sunbaked zombie is destroyed, its body explodes in a burst of stale dust.
      Adjacent creatures must succeed at a Fortitude save or be staggered for
      1d4+1 rounds. The DC is equal to 10 + 1/2 the sunbaked zombie’s Hit Dice +
      the sunbaked zombie’s Cha modifier. Creatures that don’t breathe are
      immune to this effect.<p><em>Fiery Gaze (Su):</em> A sunbaked zombie’s eye
      sockets flicker with a small flame that gives light equivalent to that of
      a candle. As a standard action, a sunbaked zombie can direct its gaze
      against a single creature within 30 feet of it. A creature targeted must
      succeed at a Fortitude save or take 1d6 points of fire damage. If the
      sunbaked zombie has 5 or more Hit Dice, its fiery gaze deals 2d6 points of
      fire damage, and this damage increases by an additional 1d6 points of fire
      damage for every 4 additional Hit Dice the sunbaked zombie possesses. A
      creature damaged by this effect must succeed at a Reflex save or catch
      fire. Each round, a burning creature can attempt a Reflex save to quench
      the flames; failure results in another 1d6 points of fire damage.
      Flammable items worn by a creature must also save or take the same damage
      as the creature. If a creature is already on fire, it suffers no
      additional effects from a fiery gaze. The save DC is
      Charisma-based.<p><b>Abilities:</b> Str +2. A sunbaked zombie has no Con
      or Int score, and its Wis and Cha become 10.<p><b>BAB:</b> A sunbaked
      zombie’s base attack bonus is equal to 3/4 of its Hit
      Dice.<p><b>Skills:</b> A sunbaked zombie has no skill
      ranks.<p><b>Feats:</b> A sunbaked zombie loses all feats possessed by the
      base creature and gains Toughness as a bonus feat.<p><b>Special
      Qualities:</b> A sunbaked zombie loses most special qualities of the base
      creature. It retains any extraordinary special qualities that improve its
      melee or ranged attacks.</p>
  subType: template
type: feat

