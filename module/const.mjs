/**
 * Action type to change context mapping.
 *
 * @see {pf1.documents.item.ItemPF.prototype.getContextChanges}
 */
export const actionTypeToContext = {
  mwak: "wmdamage",
  twak: "twdamage",
  rwak: "rwdamage",
  msak: "sdamage",
  rsak: "sdamage",
  spellsave: "sdamage",
};
