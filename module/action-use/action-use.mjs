import { ChatAttack } from "./chat-attack.mjs";
import { RollPF } from "../dice/roll.mjs";

// Documentation/type imports
/** @typedef {import("@item/item-pf.mjs").SharedActionData} SharedActionData */
/** @typedef {pf1.documents.item.ItemPF} ItemPF */
/** @typedef {pf1.documents.actor.ActorPF} ActorPF */
/** @typedef {pf1.components.ItemAction} ItemAction */

/**
 * Error states for when an item does not meet the requirements for an attack.
 *
 * @enum {number}
 * @readonly
 */
export const ERR_REQUIREMENT = Object.freeze({
  NO_ACTOR_PERM: 1,
  DISABLED: 2,
  INSUFFICIENT_QUANTITY: 3,
  INSUFFICIENT_CHARGES: 4,
  MISSING_AMMO: 5,
  INSUFFICIENT_AMMO: 6,
});

export class ActionUse {
  /**
   * The actor this action use is based on.
   *
   * @type {ActorPF}
   */
  actor;
  /**
   * The actor this action use is based on.
   *
   * @type {TokenDocument}
   */
  token;

  /**
   * The item this action use is based on.
   *
   * @type {ItemPF}
   */
  item;
  /**
   * The action this action use is based on.
   *
   * @type {ItemAction}
   */
  action;
  /**
   * The shared data object holding all relevant data for this action use.
   *
   * @type {SharedActionData}
   */
  shared;

  /**
   * @param {Partial<SharedActionData>} [shared={}] - The shared context for this action use
   */
  constructor(shared = {}) {
    Object.defineProperties(this, {
      shared: { value: shared },
      item: { value: shared.item },
      action: { value: shared.action },
      actor: { value: shared.item.actor },
      token: { value: shared.token },
    });

    // Init some shared data
    this.shared.templateData = {
      action: this.shared.action,
      item: this.shared.item,
    };
  }

  /**
   * @returns {Promise<number>} - 0 when successful, otherwise one of the ERR_REQUIREMENT constants.
   */
  checkRequirements() {
    const actor = this.item.actor;
    if (actor && !actor.isOwner) {
      ui.notifications.warn(game.i18n.format("PF1.ErrorNoActorPermissionAlt", { name: actor.name }));
      return ERR_REQUIREMENT.NO_ACTOR_PERM;
    }

    if (this.item.type === "feat" && this.item.system.disabled) {
      ui.notifications.warn(game.i18n.localize("PF1.ErrorFeatDisabled"));
      return ERR_REQUIREMENT.DISABLED;
    }

    // Cost override set to 0 or to increase charges/quantity
    if (this.shared.cost !== null && this.shared.cost <= 0) return 0;

    if (this.item.isPhysical) {
      const itemQuantity = this.item.system.quantity || 0;
      if (itemQuantity <= 0) {
        ui.notifications.warn(game.i18n.localize("PF1.ErrorNoQuantity"));
        return ERR_REQUIREMENT.INSUFFICIENT_QUANTITY;
      }
    }

    if (this.action.isSelfCharged && this.action.data.uses.self?.value < 1) {
      ui.notifications.warn(
        game.i18n.format("PF1.ErrorInsufficientCharges", {
          name: `${this.item.name}: ${this.action.name}`,
        })
      );
      return ERR_REQUIREMENT.INSUFFICIENT_CHARGES;
    }

    return 0;
  }

  /**
   * @returns {Promise<object>} The roll data object for this attack.
   */
  getRollData() {
    const rollData = foundry.utils.deepClone(this.shared.action.getRollData());
    const d20 = this.shared.dice;
    // TODO: Move this standard roll obfuscation to dialog handling
    rollData.d20 = d20 === pf1.dice.D20RollPF.standardRoll ? "" : d20;

    return rollData;
  }

  /**
   * Creates and renders an attack roll dialog, and returns a result.
   *
   * @returns {Promise<ItemAttack_Dialog_Result|boolean>}
   */
  createAttackDialog() {
    const dialog = new pf1.applications.AttackDialog(this.shared.action, this.shared.rollData, this.shared);
    return dialog.show();
  }

  /**
   * Alters roll (and shared) data based on user input during the attack's dialog.
   *
   * @param {JQuery | object} form - The attack dialog's jQuery form data or FormData object
   * @returns {Promise}
   */
  alterRollData(form = {}) {
    let formData;
    if (form instanceof jQuery) formData = new FormDataExtended(form[0].querySelector("form")).object;
    else formData = form;

    const useOptions = this.shared.useOptions;
    formData["power-attack"] ??= useOptions.powerAttack;
    formData["primary-attack"] ??= useOptions.primaryAttack;
    formData["cl-check"] ??= useOptions.clCheck;
    formData["measure-template"] ??= useOptions.measureTemplate;
    formData["haste-attack"] ??= useOptions.haste;
    formData["manyshot"] ??= useOptions.manyshot;
    formData["rapid-shot"] ??= useOptions.rapidShot;
    formData["damage-ability-multiplier"] ??= useOptions.abilityMult;

    if (formData["d20"]) this.shared.rollData.d20 = formData["d20"];
    const atkBonus = formData["attack-bonus"];
    if (atkBonus) {
      this.shared.attackBonus.push(atkBonus);
    }
    const dmgBonus = formData["damage-bonus"];
    if (dmgBonus) {
      this.shared.damageBonus.push(dmgBonus);
    }

    if (formData.rollMode) this.shared.rollMode = formData.rollMode;

    // Point-Blank Shot
    if (formData["point-blank-shot"]) {
      this.shared.attackBonus.push(`1[${game.i18n.localize("PF1.PointBlankShot")}]`);
      this.shared.damageBonus.push(`1[${game.i18n.localize("PF1.PointBlankShot")}]`);
      this.shared.pointBlankShot = true;
    }

    // Many-shot
    if (this.shared.fullAttack && formData["manyshot"]) {
      this.shared.manyShot = true;
    }

    // Rapid Shot
    if (this.shared.fullAttack && formData["rapid-shot"]) {
      this.shared.attackBonus.push(`-2[${game.i18n.localize("PF1.RapidShot")}]`);
    }

    // Primary attack
    if (formData["primary-attack"] != null)
      foundry.utils.setProperty(this.shared.rollData, "action.naturalAttack.primaryAttack", formData["primary-attack"]);

    // Use measure template
    if (formData["measure-template"] != null) this.shared.useMeasureTemplate = formData["measure-template"];

    // Set held type
    const held = formData["held"] || this.shared.rollData.action.held || this.shared.rollData.item.held || "normal";
    this.shared.rollData.item.held = held;

    // Damage multiplier
    const abilityDamageMultOverride = formData["damage-ability-multiplier"];
    if (abilityDamageMultOverride != null) {
      this.shared.rollData.action.ability.damageMult = abilityDamageMultOverride;
    }

    // Power Attack
    if (formData["power-attack"]) {
      const basePowerAttackBonus = this.shared.rollData.action?.powerAttack?.damageBonus ?? 2;
      let powerAttackBonus = (1 + Math.floor(this.shared.rollData.attributes.bab.total / 4)) * basePowerAttackBonus;

      // Get multiplier
      const paMult = this.shared.action.getPowerAttackMult({ rollData: this.shared.rollData });

      // Apply multiplier
      powerAttackBonus = Math.floor(powerAttackBonus * paMult);

      // Get label
      const label = ["rwak", "twak", "rsak"].includes(this.action.data.actionType)
        ? game.i18n.localize("PF1.DeadlyAim")
        : game.i18n.localize("PF1.PowerAttack");

      // Get penalty
      const powerAttackPenalty = -(
        1 + Math.floor(foundry.utils.getProperty(this.shared.rollData, "attributes.bab.total") / 4)
      );

      // Add bonuses
      this.shared.rollData.powerAttackPenalty = powerAttackPenalty;
      this.shared.attackBonus.push(`${powerAttackPenalty}[${label}]`);
      this.shared.powerAttack = true;
      this.shared.rollData.powerAttackBonus = powerAttackBonus;
      this.shared.rollData.powerAttackPenalty = powerAttackPenalty;
    } else {
      this.shared.rollData.powerAttackBonus = 0;
      this.shared.rollData.powerAttackPenalty = 0;
    }

    // Conditionals
    Object.keys(formData).forEach((f) => {
      const idx = f.match(/conditional\.(\d+)/)?.[1];
      if (idx && formData[f]) {
        if (!this.shared.conditionals) this.shared.conditionals = [parseInt(idx)];
        else this.shared.conditionals.push(parseInt(idx));
      }
    });

    // Apply secondary attack penalties
    if (
      this.shared.rollData.item.subType === "natural" &&
      this.shared.rollData.action?.naturalAttack.primaryAttack === false
    ) {
      const attackBonus = this.shared.rollData.action.naturalAttack?.secondary?.attackBonus || "-5";
      let damageMult = this.shared.rollData.action.naturalAttack?.secondary?.damageMult ?? 0.5;
      // Allow dialog override to work
      if (abilityDamageMultOverride) damageMult = abilityDamageMultOverride;
      this.shared.attackBonus.push(`(${attackBonus})[${game.i18n.localize("PF1.SecondaryAttack")}]`);
      this.shared.rollData.action.ability.damageMult = damageMult;
    }

    // CL check enabled
    this.shared.casterLevelCheck = formData["cl-check"];

    // Concentration enabled
    this.shared.concentrationCheck = formData["concentration"];

    // Conditional defaults for fast-forwarding
    if (!this.shared.conditionals && foundry.utils.isEmpty(formData)) {
      this.shared.conditionals = this.shared.action.data.conditionals?.reduce((arr, con, i) => {
        if (con.default) arr.push(i);
        return arr;
      }, []);
    }
  }

  /**
   * @typedef {object} ItemAttack_AttackData
   * @property {string} label - The attack's name
   * @property {number|string|undefined} [attackBonus] - An attack bonus specific to this attack
   * @property {number|string|undefined} [damageBonus] - A damage bonus specific to this attack
   * @property {string|null} [ammo] - The ID of the ammo item used
   */
  /**
   * Generates attacks for an item's action.
   *
   * @param {boolean} [forceFullAttack=false] - Generate full attack data, e.g. as base data for an {@link AttackDialog}
   * @returns {Promise<ItemAttack_AttackData[]> | ItemAttack_AttackData[]} The generated default attacks.
   */
  generateAttacks(forceFullAttack = false) {
    const rollData = this.shared.rollData;
    const action = rollData.action;

    const useOptions = this.shared.useOptions;

    /**
     * Counter for unnamed or other numbered attacks, to be incremented with each usage.
     * Starts at 1 to account for the base attack.
     */
    let unnamedAttackIndex = 1;

    const attackName =
      action.attackName || game.i18n.format("PF1.ExtraAttacks.Formula.LabelDefault", { 0: unnamedAttackIndex });
    // Use either natural fullAttack state, or force generation of all attacks via override
    const fullAttack = forceFullAttack || this.shared.fullAttack;

    const exAtkCfg = pf1.config.extraAttacks[action.extraAttacks?.type] ?? {};

    const allAttacks = [{ attackBonus: exAtkCfg.bonus || "", label: attackName }];

    // Extra attacks
    if (fullAttack) {
      const unchainedEconomy = game.settings.get("pf1", "unchainedActionEconomy");

      let attackCount = 1;

      const parseAttacks = (countFormula, bonusFormula, label, bonusLabel) => {
        rollData.bab = rollData.attributes?.bab?.total; // TODO: BAB override/modifier
        const exAtkCount = RollPF.safeRoll(countFormula, rollData)?.total ?? 0;
        if (!(exAtkCount > 0)) {
          delete rollData.bab;
          return;
        }

        try {
          for (let i = 0; i < exAtkCount; i++) {
            rollData.attackCount = attackCount += i;
            rollData.formulaicAttack = i + 1; // Add and update attack counter
            const bonus = RollPF.safeRoll(bonusFormula || "0", rollData).total;
            allAttacks.push({
              attackBonus: bonusLabel ? `(${bonus})[${bonusLabel}]` : bonus,
              // If formulaic attacks have a non-default name, number them with their own counter; otherwise, continue unnamed attack numbering
              label:
                label?.replace("{0}", i + 1) ||
                game.i18n.format("PF1.ExtraAttacks.Formula.LabelDefault", { 0: (unnamedAttackIndex += 1) }),
            });
          }
        } catch (err) {
          console.error(err);
        }

        // Cleanup roll data
        delete rollData.formulaicAttack;
        delete rollData.attackCount;
        delete rollData.bab;
      };

      if (exAtkCfg.iteratives && !unchainedEconomy)
        parseAttacks(
          pf1.config.iterativeExtraAttacks,
          pf1.config.iterativeAttackModifier,
          null,
          true,
          game.i18n.localize("PF1.Iterative")
        );

      // Add attacks defined by configuration
      if (exAtkCfg.count) parseAttacks(exAtkCfg.count, exAtkCfg.bonus);

      // Add manually entered explicit extra attacks
      if (exAtkCfg.manual) {
        const extraAttacks = action.extraAttacks?.manual ?? [];
        for (const { name, formula } of extraAttacks) {
          allAttacks.push({
            attackBonus: formula,
            // Use defined label, or fall back to continuously numbered default attack name
            label: name || game.i18n.format("PF1.ExtraAttacks.Formula.LabelDefault", { 0: (unnamedAttackIndex += 1) }),
          });
        }
      }

      // Add custom extra attack formula
      if (exAtkCfg.formula) {
        parseAttacks(
          action.extraAttacks.formula?.count,
          action.extraAttacks.formula?.bonus,
          action.extraAttacks.formula?.label
        );
      }
    }

    // Set ammo usage
    const ammoType = this.action.ammo?.type;
    if (ammoType) {
      const ammoId = this.item.getFlag("pf1", "defaultAmmo");
      const item = this.item.actor?.items.get(ammoId);
      const quantity = item?.system.quantity || 0;
      const ammoCost = this.action.ammoCost;
      const abundant = item?.flags.pf1?.abundant ?? false;
      for (let a = 0; a < allAttacks.length; a++) {
        const atk = allAttacks[a];
        if (abundant || quantity >= a + ammoCost) atk.ammo = ammoId;
        else atk.ammo = null;
      }
    }

    return allAttacks;
  }

  async autoSelectAmmo() {
    const ammoType = this.shared.action.ammoType;
    if (!ammoType) return;

    const ammoCost = this.action.ammoCost;

    const ammoId = this.item.getFlag("pf1", "defaultAmmo");
    const item = this.item.actor?.items.get(ammoId);
    if (item && (item.system.quantity || 0) >= ammoCost) return;

    const ammo = this.actor.itemTypes.loot
      .filter(
        (i) =>
          i.subType === "ammo" &&
          i.system.extraType === ammoType &&
          i.system.quantity >= ammoCost &&
          i.system.identified !== false
      )
      .sort((a, b) => a.system.price - b.system.price);

    if (ammo.length == 0) return;

    await this.item.setFlag("pf1", "defaultAmmo", ammo[0].id);
  }

  /**
   * Subtracts ammo for this attack, updating relevant items with new quantities.
   *
   * @param {number} [value=1] - How much ammo to subtract.
   * @returns {Promise}
   */
  async subtractAmmo(value = 1) {
    if (!this.shared.action.ammoType) return;

    const actor = this.item.actor;

    const ammoUsage = {};
    for (const atk of this.shared.attacks) {
      if (atk.ammo) {
        const item = actor.items.get(atk.ammo);
        // Don't remove abundant ammunition
        if (item.flags?.pf1?.abundant) continue;

        ammoUsage[atk.ammo] ??= 0;
        ammoUsage[atk.ammo] += value;
      }
    }

    this.shared.ammoUsage = ammoUsage;

    if (!foundry.utils.isEmpty(ammoUsage)) {
      const updateData = Object.entries(ammoUsage).reduce((cur, o) => {
        const currentValue = this.item.actor.items.get(o[0]).system.quantity;
        const obj = {
          _id: o[0],
          "system.quantity": currentValue - o[1],
        };

        cur.push(obj);
        return cur;
      }, []);

      return this.item.actor.updateEmbeddedDocuments("Item", updateData);
    }
  }

  /**
   * Update remaining ammo in {@link ChatAttack}s
   */
  updateAmmoUsage() {
    const actor = this.actor;
    const ammoCost = this.action.ammoCost;
    for (const atk of this.shared.attacks) {
      if (!atk.ammo) continue;
      const attack = atk.chatAttack;
      const ammo = actor.items.get(atk.ammo)?.system.quantity || 0;
      attack.ammo.remaining = ammo;
      attack.ammo.quantity = ammoCost;
    }
  }

  handleConditionals() {
    if (this.shared.conditionals) {
      const conditionalData = {};
      for (const i of this.shared.conditionals) {
        const conditional = this.shared.action.data.conditionals[i];
        const tag = pf1.utils.createTag(conditional.name);
        for (const [i, modifier] of conditional.modifiers.entries()) {
          // Adds a formula's result to rollData to allow referencing it.
          // Due to being its own roll, this will only correctly work for static formulae.
          const conditionalRoll = RollPF.safeRoll(modifier.formula, this.shared.rollData);
          if (conditionalRoll.err) {
            ui.notifications.warn(
              game.i18n.format("PF1.WarningConditionalRoll", { number: i + 1, name: conditional.name })
            );
            // Skip modifier to avoid multiple errors from one non-evaluating entry
            continue;
          } else conditionalData[[tag, i].join(".")] = RollPF.safeRoll(modifier.formula, this.shared.rollData).total;

          // Create a key string for the formula array
          const partString = `${modifier.target}.${modifier.subTarget}${
            modifier.critical ? "." + modifier.critical : ""
          }`;
          // Add formula in simple format
          if (["attack", "effect", "misc"].includes(modifier.target)) {
            const hasFlavor = /\[.*\]/.test(modifier.formula);
            const flavoredFormula = hasFlavor ? modifier.formula : `(${modifier.formula})[${conditional.name}]`;
            this.shared.conditionalPartsCommon[partString] = [
              ...(this.shared.conditionalPartsCommon[partString] ?? []),
              flavoredFormula,
            ];
          }
          // Add formula as array for damage
          else if (modifier.target === "damage") {
            this.shared.conditionalPartsCommon[partString] = [
              ...(this.shared.conditionalPartsCommon[partString] ?? []),
              [modifier.formula, modifier.damageType, false],
            ];
          }
          // Add formula to the size property
          else if (modifier.target === "size") {
            this.shared.rollData.size += conditionalRoll.total;
          }
        }
      }
      // Expand data into rollData to enable referencing in formulae
      this.shared.rollData.conditionals = foundry.utils.expandObject(conditionalData, 5);

      // Add specific pre-rolled rollData entries
      for (const target of ["effect.cl", "effect.dc", "misc.charges"]) {
        if (this.shared.conditionalPartsCommon[target] != null) {
          const formula = this.shared.conditionalPartsCommon[target].join("+");
          const roll = RollPF.safeRoll(formula, this.shared.rollData, [target, formula]).total;
          switch (target) {
            case "effect.cl":
              this.shared.rollData.cl += roll;
              break;
            case "effect.dc":
              this.shared.rollData.dcBonus ||= 0;
              this.shared.rollData.dcBonus += roll;
              break;
            case "misc.charges":
              this.shared.rollData.chargeCostBonus ||= 0;
              this.shared.rollData.chargeCostBonus += roll;
              break;
          }
        }
      }
    }
  }

  /**
   * Checks all requirements to make the attack. This is after the attack dialog's data has been parsed.
   *
   * @returns {Promise<number> | number} 0 if successful, otherwise one of the ERR_REQUIREMENT constants.
   */
  checkAttackRequirements() {
    // Determine charge cost
    const baseCost = this.shared.action.getChargeCost({ rollData: this.shared.rollData });

    // Bonus cost, e.g. from a conditional modifier
    const bonusCost = this.shared.rollData["chargeCostBonus"] ?? 0;

    let cost = baseCost + bonusCost;

    // Override cost
    if (this.shared.cost !== null) cost = this.shared.cost;

    // Save chargeCost as rollData entry for anything else
    this.shared.rollData.chargeCost = cost;

    if (cost > 0) {
      const uses = this.item.charges;
      if (this.item.type === "spell") {
        if (this.item.spellbook?.spontaneous && !this.item.system.preparation?.value) {
          cost = Infinity;
        }
      }

      // Cancel usage on insufficient charges
      if (cost > uses) {
        ui.notifications.warn(game.i18n.format("PF1.ErrorInsufficientCharges", { name: this.item.name }));
        return ERR_REQUIREMENT.INSUFFICIENT_CHARGES;
      }
    }

    return 0;
  }

  /**
   * Generates ChatAttack entries based off the attack type.
   */
  async generateChatAttacks() {
    // Normal attack(s)
    if (this.shared.action.hasAttack) await this.addAttacks();
    // Damage only
    else if (this.shared.action.hasDamage) await this.addDamage();
    // Effect notes only
    else await this.addEffectNotes();

    // Add footnotes
    await this.addFootnotes();

    // Add attack cards
    this.shared.attacks.forEach((attack) => {
      if (!attack.ammo) return;
      /** @type {ChatAttack} */
      const atk = attack.chatAttack;
      if (atk) atk.setAmmo(attack.ammo);
    });

    // Add save info
    this.shared.save = this.shared.action.data.save.type;
    this.shared.saveDC = this.shared.action.getDC(this.shared.rollData);
  }

  /**
   * Determines conditional parts used in a specific attack.
   *
   * @param {object} atk - The attack used.
   * @param {number} [index=0] - The index of the attack, in order of enabled attacks.
   * @returns {object} The conditional parts used.
   */
  _getConditionalParts(atk, { index = 0 }) {
    const result = {};

    const conditionalTemplates = {
      "attack.normal": "attack.;id;.normal",
      "attack.crit": "attack.;id;.crit",
      "damage.normal": "damage.;id;.normal",
      "damage.crit": "damage.;id;.crit",
      "damage.nonCrit": "damage.;id;.nonCrit",
    };
    const addPart = (id) => {
      for (const [templateKey, templateStr] of Object.entries(conditionalTemplates)) {
        if (!result[templateKey]) result[templateKey] = [];

        const parsedStr = templateStr.replace(";id;", id);
        result[templateKey].push(...(this.shared.conditionalPartsCommon[parsedStr] ?? []));
      }
    };

    addPart(`attack_${index}`);
    addPart("allAttack");
    addPart("allDamage");

    if (atk.id === "rapid-shot") {
      addPart("rapidShotAttack");
      addPart("rapidShotDamage");
    } else if (atk.id === "haste-attack") {
      addPart("hasteAttack");
      addPart("hasteDamage");
    }

    return result;
  }

  /**
   * Adds ChatAttack entries to an attack's shared context.
   */
  async addAttacks() {
    for (let a = 0; a < this.shared.attacks.length; a++) {
      const atk = this.shared.attacks[a];

      // Combine conditional modifiers for attack and damage
      const conditionalParts = this._getConditionalParts(atk, { index: a });

      this.shared.rollData.attackCount = a;

      // Create attack object
      const attack = new ChatAttack(this.shared.action, {
        label: atk.label,
        rollData: this.shared.rollData,
        targets: game.user.targets,
      });

      if (atk.id !== "manyshot") {
        // Add attack roll
        await attack.addAttack({
          extraParts: [...this.shared.attackBonus, atk.attackBonus],
          conditionalParts,
        });
      }

      // Add damage
      if (this.shared.action.hasDamage) {
        const extraParts = foundry.utils.deepClone(this.shared.damageBonus);
        const nonCritParts = [];
        const critParts = [];

        // Add power attack bonus
        if (this.shared.rollData.powerAttackBonus > 0) {
          // Get label
          const label = ["rwak", "twak", "rsak"].includes(this.shared.action.data.actionType)
            ? game.i18n.localize("PF1.DeadlyAim")
            : game.i18n.localize("PF1.PowerAttack");

          const powerAttackBonus = this.shared.rollData.powerAttackBonus;
          const powerAttackCritBonus =
            powerAttackBonus * (this.shared.rollData.action?.powerAttack?.critMultiplier ?? 1);
          nonCritParts.push(`${powerAttackBonus}[${label}]`);
          critParts.push(`${powerAttackCritBonus}[${label}]`);
        }

        // Add damage
        let flavor = null;
        if (atk.id === "manyshot") flavor = game.i18n.localize("PF1.Manyshot");
        await attack.addDamage({
          flavor,
          extraParts: [...extraParts, ...nonCritParts],
          critical: false,
          conditionalParts,
        });

        // Add critical hit damage
        if (attack.hasCritConfirm) {
          await attack.addDamage({ extraParts: [...extraParts, ...critParts], critical: true, conditionalParts });
        }
      }

      // Add effect notes
      if (atk.id !== "manyshot") {
        await attack.addEffectNotes();
      }

      // Add to list
      this.shared.chatAttacks.push(attack);

      // Add a reference to the attack that created this chat attack
      atk.chatAttack = attack;
    }

    // Cleanup rollData
    delete this.shared.rollData.attackCount;
  }

  /**
   * Adds a ChatAttack entry for damage to an attack's shared context.
   */
  async addDamage() {
    // Set conditional modifiers
    this.shared.conditionalParts = {
      "damage.normal": this.shared.conditionalPartsCommon["damage.allDamage.normal"] ?? [],
    };

    const attack = new ChatAttack(this.shared.action, {
      rollData: this.shared.rollData,
      primaryAttack: this.shared.primaryAttack,
    });
    // Add damage
    await attack.addDamage({
      extraParts: foundry.utils.deepClone(this.shared.damageBonus),
      critical: false,
      conditionalParts: this.shared.conditionalParts,
    });

    // Add effect notes
    await attack.addEffectNotes();

    // Add to list
    this.shared.chatAttacks.push(attack);
  }

  async addFootnotes() {
    if (!this.item) return;

    const type = this.action.data.actionType;
    const typeMap = {
      rsak: ["ranged", "rangedSpell"],
      rwak: ["ranged", "rangedWeapon"],
      twak: ["ranged", "thrownWeapon", "rangedWeapon"],
      rcman: ["ranged"],
      mwak: ["melee", "meleeWeapon"],
      msak: ["melee", "meleeSpell"],
      mcman: ["melee"],
    };

    const notes = [];
    // Add actor notes
    if (this.actor) {
      notes.push(...this.item.actor.getContextNotesParsed("attacks.attack"));
      typeMap[type]?.forEach((subTarget) =>
        notes.push(...this.item.actor.getContextNotesParsed(`attacks.${subTarget}`))
      );
    }
    // Add item notes
    if (this.item?.system.attackNotes) {
      notes.push(...this.item.system.attackNotes);
    }
    // Add action notes
    if (this.action.data.attackNotes) {
      notes.push(...this.action.data.attackNotes);
    }
    // Add CMB notes
    if (this.action.isCombatManeuver) {
      notes.push(...(this.item?.actor?.getContextNotesParsed("misc.cmb") ?? []));
    }

    if (this.hasCritConfirm) {
      notes.push(...(this.action.actor?.getContextNotesParsed("attacks.critical") ?? []));
    }

    this.shared.templateData.footnotes = notes;
  }

  /**
   * Adds a ChatAttack entry for effect notes to an attack's shared context.
   */
  async addEffectNotes() {
    const attack = new ChatAttack(this.shared.action, {
      rollData: this.shared.rollData,
      primaryAttack: this.shared.primaryAttack,
    });

    // Add effect notes
    await attack.addEffectNotes();

    // Add to list
    this.shared.chatAttacks.push(attack);
  }

  /**
   * @typedef {object} Attack_MeasureTemplateResult
   * @property {boolean} result - Whether an area was selected.
   * @property {Function} [place] - Function to place the template, if an area was selected.
   * @property {Function} [delete] - Function to delete the template, if it has been placed.
   */
  /**
   * Prompts the user for an area, based on the attack's measure template.
   *
   * @returns {Promise.<Attack_MeasureTemplateResult>} Whether an area was selected.
   */
  async promptMeasureTemplate() {
    const mt = this.shared.action.data.measureTemplate;

    // Determine size
    let dist = RollPF.safeRoll(mt.size, this.shared.rollData).total;
    // Apply system of units conversion
    dist = pf1.utils.convertDistance(dist)[0];

    // Create data object
    const templateOptions = {
      type: mt.type,
      distance: dist,
      flags: {
        pf1: {
          origin: {
            uuid: this.shared.item.uuid,
            action: this.shared.action.id,
          },
        },
      },
    };

    if (mt.color) {
      // Color transformation to avoid invalid colors provided by user from corrupting the template
      const c = Color.fromString(mt.color);
      if (Number.isFinite(Number(c))) templateOptions.color = c.toString();
    }
    if (mt.texture) templateOptions.texture = mt.texture;

    // Create template
    this.shared.template = null;
    const template = pf1.canvas.AbilityTemplate.fromData(templateOptions);
    let result;
    if (template) {
      const actorSheet = this.item.actor?.sheet;
      const sheetRendered = actorSheet?._element != null;
      if (sheetRendered) actorSheet.minimize();
      result = await template.drawPreview(this.shared.event);
      if (!result.result) {
        if (sheetRendered) actorSheet.maximize();
        return result;
      }
    }

    this.shared.template = await result.place();
    return result;
  }

  /**
   * Handles Dice So Nice integration.
   */
  async handleDiceSoNice() {
    if (!game.settings.get("pf1", "integration").diceSoNice) return;
    if (!game.dice3d?.isEnabled()) return;

    // Use try to make sure a chat card is rendered even if DsN fails
    try {
      // Define common visibility options for whole attack
      const chatData = {};
      ChatMessage.implementation.applyRollMode(chatData, this.shared.rollMode);

      const mergeRolls = game.settings.get("dice-so-nice", "enabledSimultaneousRolls");
      const skipRolls = game.settings.get("dice-so-nice", "immediatelyDisplayChatMessages");

      /**
       * Visually roll dice
       *
       * @async
       * @param {PoolTerm[]} pools - An array of PoolTerms to be rolled together
       * @returns {Promise} A Promise that is resolved when all rolls have been displayed
       */
      const showRoll = async (pools) => {
        const whisper = chatData.whisper?.length ? chatData.whisper : undefined; // DSN does not like empty array for whisper
        if (mergeRolls) {
          return Promise.all(
            pools.map((pool) => game.dice3d.showForRoll(pool, game.user, true, whisper, chatData.blind))
          );
        } else {
          for (const pool of pools) {
            await game.dice3d.showForRoll(pool, game.user, true, whisper, chatData.blind);
          }
        }
      };

      /** @type {PoolTerm[]} */
      const pools = [];

      for (const atk of this.shared.chatAttacks) {
        // Create PoolTerm for attack and damage rolls
        const attackPool = new PoolTerm();
        if (atk.attack) attackPool.rolls.push(atk.attack);
        attackPool.rolls.push(...(atk.damage?.rolls ?? []));

        // Create PoolTerm for crit confirmation and crit damage rolls
        const critPool = new PoolTerm();
        if (atk.hasCritConfirm) critPool.rolls.push(atk.critConfirm);
        critPool.rolls.push(...(atk.critDamage?.rolls ?? []));

        // Add non-empty pools to the array of rolls to be displayed
        if (attackPool.rolls.length) pools.push(attackPool);
        if (critPool.rolls.length) pools.push(critPool);
      }

      if (pools.length) {
        // Chat card is to be shown immediately
        if (skipRolls) showRoll(pools);
        // Wait for rolls to finish before showing the chat card
        else await showRoll(pools);
      }
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Adds an attack's chat card data to the shared object.
   */
  async getMessageData() {
    if (this.shared.chatAttacks.length === 0) return;

    // Create chat template data
    this.shared.templateData = {
      ...this.shared.templateData,
      name: this.item.name,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      rollMode: this.shared.rollMode,
      attacks: this.shared.chatAttacks.map((o) => o.finalize()),
    };

    const actor = this.item.actor,
      token = this.token ?? actor?.token;

    // Set chat data
    this.shared.chatData = {
      speaker: ChatMessage.implementation.getSpeaker({ actor, token, alias: token?.name }),
      rollMode: this.shared.rollMode,
    };

    // Set attack sound
    if (this.shared.action.data.soundEffect) this.shared.chatData.sound = this.shared.action.data.soundEffect;
    // Set dice sound if neither attack sound nor Dice so Nice are available
    else if (!game.settings.get("pf1", "integration").diceSoNice || !game.dice3d?.isEnabled())
      this.shared.chatData.sound = CONFIG.sounds.dice;

    // Get extra text
    const props = [];
    const extraText = await this.enrichNotes(this.shared.templateData.footnotes, "PF1.Footnotes", "footnotes");

    const itemChatData = await this.item.getChatData({
      actionId: this.shared.action.id,
      chatcard: true,
      rollData: this.shared.rollData,
    });

    // Get properties
    const properties = [...itemChatData.properties, ...this.addGenericPropertyLabels()];
    if (properties.length > 0) props.push({ header: game.i18n.localize("PF1.InfoShort"), value: properties });

    // Get combat properties
    if (game.combat) {
      const combatProps = this.addCombatPropertyLabels();

      if (combatProps.length > 0) {
        props.push({
          header: game.i18n.localize("PF1.CombatInfo_Header"),
          value: combatProps,
          css: "combat-properties",
        });
      }
    }

    // Add CL notes
    if (this.item.type === "spell" && actor) {
      const clNotes = actor.getContextNotesParsed(`spell.cl.${this.item.system.spellbook}`);

      if (clNotes.length) {
        props.push({
          header: game.i18n.localize("PF1.CLNotes"),
          value: clNotes,
        });
      }
    }

    // Parse template data
    const identified = Boolean(this.shared.rollData.item?.identified ?? true);
    const name = identified ? `${this.item.name} (${this.shared.action.name})` : this.item.getName(true);
    this.shared.templateData = {
      ...this.shared.templateData,
      tokenUuid: token?.uuid,
      actionId: this.shared.action?.id,
      extraText: extraText,
      identified: identified,
      name: name,
      description: identified ? itemChatData.identifiedDescription : itemChatData.unidentifiedDescription,
      actionDescription: itemChatData.actionDescription,
      properties: props,
      item: this.item.toObject(),
      actor,
      token,
      scene: canvas.scene?.id,
      hasSave: this.shared.action.hasSave,
      rollData: this.shared.rollData,
      save: {
        dc: this.shared.saveDC,
        type: this.shared.save,
        label: game.i18n.format("PF1.SavingThrowButtonLabel", {
          type: pf1.config.savingThrows[this.shared.save],
          dc: this.shared.saveDC.toString(),
        }),
        gmSensitiveLabel: game.i18n.format("PF1.SavingThrowButtonLabelGMSensitive", {
          save: pf1.config.savingThrows[this.shared.save],
        }),
      },
    };

    // Add range info
    {
      const range = this.shared.action.getRange({ type: "max", rollData: this.shared.rollData });
      if (range != null) {
        this.shared.templateData.range = range;
        const usystem = pf1.utils.getDistanceSystem();
        this.shared.templateData.rangeLabel = usystem === "metric" ? `${range} m` : `${range} ft.`;

        const rangeUnits = this.shared.action.data.range.units;
        if (["melee", "touch", "reach", "close", "medium", "long"].includes(rangeUnits)) {
          this.shared.templateData.rangeLabel = pf1.config.distanceUnits[rangeUnits];
        }
      }
    }

    // Add spell info
    if (this.item.type === "spell" && actor) {
      // Spell failure
      if (actor.spellFailure > 0 && this.item.system.components.somatic) {
        const spellbook = foundry.utils.getProperty(
          actor.system,
          `attributes.spells.spellbooks.${this.item.system.spellbook}`
        );
        if (spellbook && spellbook.arcaneSpellFailure) {
          const roll = RollPF.safeRoll("1d100");
          this.shared.templateData.spellFailure = roll.total;
          this.shared.templateData.spellFailureRoll = roll;
          this.shared.templateData.spellFailureSuccess = this.shared.templateData.spellFailure > actor.spellFailure;
        }
      }
      // Caster Level Check
      this.shared.templateData.casterLevelCheck = this.shared.casterLevelCheck;
      // Concentration check
      this.shared.templateData.concentrationCheck = this.shared.concentrationCheck;
    }

    // Generate metadata
    const metadata = this.generateChatMetadata();

    // Get target info
    if (!game.settings.get("pf1", "disableAttackCardTargets")) {
      this.shared.templateData.targets = this.shared.targets.map((t) => ({
        img: t.document.texture.src,
        actorData: t.actor?.toObject(false),
        tokenData: t.document.toObject(false),
        uuid: t.document.id,
      }));
    }

    this.shared.chatData["flags.pf1.metadata"] = metadata;
    this.shared.chatData["flags.core.canPopout"] = true;
    if (!identified)
      this.shared.chatData["flags.pf1.identifiedInfo"] = {
        identified,
        name: this.item._source.name || this.item.name,
        description: itemChatData.identifiedDescription,
        actionName: this.shared.action.name,
        actionDescription: itemChatData.actionDescription,
      };
  }

  /**
   * Enrich notes
   *
   * @param {Array<string>} notes - Notes
   * @param {string} title - Notes section title
   * @param {string} css - CSS selectors
   * @returns {string} - Enriched HTML as text
   */
  async enrichNotes(notes, title, css) {
    if (notes.length === 0) return;

    const enrichOptions = {
      rollData: this.shared.rollData,
      async: true,
      relativeTo: this.actor,
    };

    const renderContext = {
      notes,
      css,
      title,
    };

    const content = await renderTemplate("systems/pf1/templates/chat/parts/item-notes.hbs", renderContext);

    return TextEditor.enrichHTML(content, enrichOptions);
  }

  /**
   * Adds generic property labels to an attack's chat card.
   *
   * @returns {string[]} The resulting property labels.
   */
  addGenericPropertyLabels() {
    const properties = [];

    // Add actual cost
    const cost = this.shared.rollData.chargeCost;
    if (cost && !this.item.system.atWill) {
      if (this.item.type === "spell" && this.item.useSpellPoints()) {
        properties.push(`${game.i18n.localize("PF1.SpellPointsCost")}: ${cost}`);
      } else {
        properties.push(`${game.i18n.localize("PF1.ChargeCost")}: ${cost}`);
      }
    }

    // Add conditions
    const conditions = Object.entries(this.actor.system.conditions ?? {})
      .filter(([_, enabled]) => enabled)
      .map(([id]) => pf1.registry.conditions.get(id))
      .filter((c) => c?.showInAction)
      .map((c) => c.name);

    // Special case
    if (this.actor.system.conditions?.deaf && this.item.type === "spell") {
      // TODO: Check if someone modified the conditions to show anyway?
      conditions.push(pf1.registry.conditions.get("deaf").name);
    }

    if (conditions.length) properties.push(...conditions);

    // Add info for broken state
    if (this.shared.rollData.item.broken) {
      properties.push(game.i18n.localize("PF1.Broken"));
    }

    // Nonlethal
    if (this.action.data.nonlethal) properties.push(game.i18n.localize("PF1.Nonlethal"));

    if (this.action.data.touch) properties.push(game.i18n.localize("PF1.TouchAttackShort"));

    // Add info for material
    let materialKey = null;
    let materialAddons = null;
    const normalMaterialAction = this.action.data.material?.normal.value;
    const normalMaterialItem = this.item.system.material?.normal.value;
    const baseMaterialItem = this.item.system.material?.base?.value;
    const addonMaterialAction = this.action.data.material?.addon;
    const addonMaterialItem = this.item.system.material?.addon;

    // Check the action data first, then the normal material, then the base material
    if (normalMaterialAction && normalMaterialAction.length > 0) materialKey = normalMaterialAction;
    else if (normalMaterialItem && normalMaterialItem.length > 0) materialKey = normalMaterialItem;
    else materialKey = baseMaterialItem;

    if (materialKey) {
      properties.push(pf1.registry.materialTypes.get(materialKey.toLowerCase())?.name ?? materialKey.capitalize());
    }

    // Check for addon materials; prefer action data, then item data
    if (addonMaterialAction && addonMaterialAction.length > 0) materialAddons = addonMaterialAction;
    else if (addonMaterialItem && addonMaterialItem.length > 0) materialAddons = addonMaterialItem;

    if (materialAddons) {
      const materialAddonNames = materialAddons
        .map((addon) => {
          if (!addon) return null;
          const addonName = pf1.registry.materialTypes.get(addon.toLowerCase())?.name ?? addon.capitalize();
          return addonName;
        })
        .filter((addon) => !!addon);

      if (materialAddonNames.length > 0) properties.push(...materialAddonNames);
    }

    // Add info for alignments
    const actionAlignments = this.action.data.alignments;
    const itemAlignments = this.item.system.alignments ?? {};
    if (actionAlignments) {
      for (const alignment of Object.keys(actionAlignments)) {
        if (
          actionAlignments[alignment] || // The action alignment is true OR
          (actionAlignments[alignment] === null && // (The action alignment is indeterminate AND
            itemAlignments[alignment])
        ) {
          // The item has the alignment)
          properties.push(game.i18n.localize(`PF1.Alignments.${alignment[0]}`));
        }
      }
    } else {
      // Honestly, this should never happen. An action should always have an alignment now.
      for (const alignment of Object.keys(itemAlignments)) {
        if (itemAlignments[alignment]) {
          properties.push(game.i18n.localize(`PF1.Alignments.${alignment[0]}`));
        }
      }
    }

    // Add info for Power Attack to melee, Deadly Aim to ranged attacks
    if (this.shared.powerAttack) {
      switch (this.action.data.actionType) {
        case "rwak":
        case "twak":
          properties.push(game.i18n.localize("PF1.DeadlyAim"));
          break;
        case "mwak":
          properties.push(game.i18n.localize("PF1.PowerAttack"));
          break;
      }
    }

    // Add info for Point-Blank shot
    if (this.shared.pointBlankShot) properties.push(game.i18n.localize("PF1.PointBlankShot"));

    // Add info for Rapid Shot
    if (this.shared.attacks.find((o) => o.id === "rapid-shot")) properties.push(game.i18n.localize("PF1.RapidShot"));

    if (this.shared.manyShot) properties.push(game.i18n.localize("PF1.Manyshot"));

    // Add Armor Check Penalty's application to attack rolls info
    if (this.item.hasAttack && this.shared.rollData.attributes?.acp?.attackPenalty > 0)
      properties.push(game.i18n.localize("PF1.ArmorCheckPenalty"));

    // Add conditionals info
    if (this.shared.conditionals?.length) {
      this.shared.conditionals.forEach((c) => {
        properties.push(this.shared.action.data.conditionals[c].name);
      });
    }

    // Add Wound Thresholds info
    if (this.shared.rollData.attributes?.woundThresholds?.level > 0)
      properties.push(
        game.i18n.localize(pf1.config.woundThresholdConditions[this.shared.rollData.attributes.woundThresholds.level])
      );

    return properties;
  }

  /**
   * Adds combat property labels to an attack's chat card.
   *
   * @returns {string[]} The resulting property labels.
   */
  addCombatPropertyLabels() {
    const properties = [];

    // Add round info
    properties.push(game.i18n.format("PF1.CombatInfo_Round", { round: game.combat.round }));

    return properties;
  }

  /**
   * Generates metadata for this attack for the chat card to store.
   *
   * @returns {object} The resulting metadata object.
   */
  generateChatMetadata() {
    const metadata = {};

    metadata.item = this.item.id;
    metadata.action = this.action.id;
    if (this.actor && game.combat?.combatants.some((c) => c.actor === this.actor)) {
      metadata.combat = game.combat.id;
    }
    metadata.template = this.shared.template?.id ?? null;
    metadata.rolls = {
      attacks: [],
    };

    metadata.targets = this.shared.targets.map((t) => t.document.uuid);

    // Add attack rolls
    for (let attackIndex = 0; attackIndex < this.shared.chatAttacks.length; attackIndex++) {
      const chatAttack = this.shared.chatAttacks[attackIndex];
      const attackRolls = { attack: null, damage: [], critConfirm: null, critDamage: [] };
      // Add attack roll
      if (chatAttack.attack) attackRolls.attack = chatAttack.attack.toJSON();
      // Add damage rolls
      if (chatAttack.damage.rolls.length) {
        for (let damageIndex = 0; damageIndex < chatAttack.damage.rolls.length; damageIndex++) {
          const damageRoll = chatAttack.damage.rolls[damageIndex];
          attackRolls.damage[damageIndex] = damageRoll.toJSON();
        }
      }
      // Add critical confirmation roll
      if (chatAttack.critConfirm) attackRolls.critConfirm = chatAttack.critConfirm.toJSON();
      // Add critical damage rolls
      if (chatAttack.critDamage.rolls.length) {
        for (let damageIndex = 0; damageIndex < chatAttack.critDamage.rolls.length; damageIndex++) {
          const damageRoll = chatAttack.critDamage.rolls[damageIndex];
          attackRolls.critDamage[damageIndex] = damageRoll.toJSON();
        }
      }

      // Record used ammo ID and quantity
      if (chatAttack.ammo?.id) {
        // Quantity is included for future proofing for supporting attacks that consume more than 1.
        attackRolls.ammo = { id: chatAttack.ammo.id, quantity: 1 };
      }

      metadata.rolls.attacks[attackIndex] = attackRolls;
    }

    // Add miscellaneous metadata
    if (this.shared.saveDC) metadata.save = { dc: this.shared.saveDC, type: this.shared.save };
    if (this.item.type === "spell") metadata.spell = { cl: this.shared.rollData.cl, sl: this.shared.rollData.sl };

    return metadata;
  }

  /**
   * Executes the item's script calls.
   *
   * @param {"use"|"postUse"} [category="use"] Script call category
   */
  async executeScriptCalls(category = "use") {
    const shared = this.shared;

    if (!("attackData" in shared)) {
      Object.defineProperty(shared, "attackData", {
        get: () => {
          foundry.utils.logCompatibilityWarning(
            "shared.attackData is deprecated in favor of directly accessing shared",
            {
              since: "PF1 vNEXT",
              until: "PF1 vNEXT+2",
            }
          );
          return shared;
        },
      });
    }

    const rv = await this.item.executeScriptCalls(category, {}, shared);

    if (category === "use") this.shared.scriptData = rv;
  }

  /**
   * Posts the attack's chat card.
   *
   * @returns {Promise<ChatMessage | SharedActionData | { descriptionOnly: boolean }> }
   */
  async postMessage() {
    // Old hook data + callAll
    const hookData = {
      ev: this.shared.event,
      skipDialog: this.shared.skipDialog,
      chatData: this.shared.chatData,
      templateData: this.shared.templateData,
      shared: this.shared,
    };

    this.shared.chatTemplate ||= "systems/pf1/templates/chat/attack-roll.hbs";
    this.shared.templateData.damageTypes = pf1.registry.damageTypes.toObject();
    if (Hooks.call("pf1PreDisplayActionUse", this) === false) return;

    // Show chat message
    let result;
    if (this.shared.chatAttacks.length > 0) {
      if (this.shared.chatMessage && this.shared.scriptData.hideChat !== true) {
        const enrichOptions = {
          rollData: this.shared.rollData,
          secrets: this.isOwner,
          async: true,
          relativeTo: this.actor,
        };

        const content = await renderTemplate(this.shared.chatTemplate, this.shared.templateData);
        this.shared.chatData.content = await TextEditor.enrichHTML(content, enrichOptions);

        const hiddenData = this.shared.chatData["flags.pf1.identifiedInfo"];
        if (hiddenData?.description) {
          hiddenData.description = await TextEditor.enrichHTML(hiddenData.description, enrichOptions);
        }
        if (hiddenData?.actionDescription) {
          hiddenData.actionDescription = await TextEditor.enrichHTML(hiddenData.actionDescription, enrichOptions);
        }

        // Apply roll mode
        this.shared.chatData.rollMode ??= game.settings.get("core", "rollMode");
        ChatMessage.implementation.applyRollMode(this.shared.chatData, this.shared.chatData.rollMode);

        result = await ChatMessage.implementation.create(this.shared.chatData);
      } else result = this.shared;
    } else {
      if (this.shared.chatMessage && this.shared.scriptData.hideChat !== true) result = this.item.roll();
      else result = { descriptionOnly: true };
    }

    return result;
  }

  /**
   * Collect valid targets.
   */
  getTargets() {
    // Get targets from template, and if no template is present, from explicitly targeted tokens list
    const targets = this.shared.template?.object.getTokensWithin() ?? Array.from(game.user.targets);

    // Ignore defeated and secret tokens
    this.shared.targets = targets.filter(
      (t) => t.document.disposition !== CONST.TOKEN_DISPOSITIONS.SECRET && t.combatant?.isDefeated !== true
    );
  }

  /**
   * Process everything
   *
   * @param {object} [options] - Additional options
   * @param {boolean} [options.skipDialog=false] - Skip dialog
   * @returns {Promise<ChatMessage|SharedActionData|void>}
   */
  async process({ skipDialog = false } = {}) {
    const shared = this.shared;

    // Check requirements for item
    let reqErr = await this.checkRequirements();
    if (reqErr > 0) return { err: pf1.actionUse.ERR_REQUIREMENT, code: reqErr };

    await this.autoSelectAmmo();

    // Get new roll data
    shared.rollData = this.getRollData();

    // let modules modify the ActionUse before attacks are rolled
    Hooks.callAll("pf1CreateActionUse", this);

    // Show attack dialog, if appropriate
    if (!skipDialog) {
      const result = await this.createAttackDialog();

      // Stop if result is not an object (i.e. when closed is clicked on the dialog)
      if (result === null) return;

      // Alter roll data
      shared.fullAttack = result.fullAttack;
      shared.attacks = result.attacks;
      await this.alterRollData(result.html);
    } else {
      shared.attacks = await this.generateAttacks();
      await this.alterRollData();
    }

    // Filter out attacks without ammo usage
    if (shared.action.ammoType) {
      shared.attacks = shared.attacks.filter((o) => o.ammo != null);
      if (shared.attacks.length === 0) {
        ui.notifications.error(game.i18n.localize("PF1.AmmoDepleted"));
        return;
      }
    }

    // Limit attacks to 1 if not full rounding
    if (!shared.fullAttack) shared.attacks = shared.attacks.slice(0, 1);
    // Handle conditionals
    await this.handleConditionals();

    // Check attack requirements, post-dialog
    reqErr = await this.checkAttackRequirements();
    if (reqErr > 0) return { err: pf1.actionUse.ERR_REQUIREMENT, code: reqErr };

    // Generate chat attacks
    await this.generateChatAttacks();

    // Prompt measure template
    let measureResult;
    if (shared.useMeasureTemplate && canvas.scene) {
      measureResult = await this.promptMeasureTemplate();
      if (!measureResult.result) return;
    }

    // Call itemUse hook and determine whether the item can be used based off that
    if (Hooks.call("pf1PreActionUse", this) === false) {
      await measureResult?.delete();
      return;
    }

    this.getTargets();

    // Call script calls
    await this.executeScriptCalls();
    if (shared.scriptData?.reject) {
      await measureResult?.delete();
      return;
    }

    const premessage_promises = [];
    // Handle Dice So Nice
    premessage_promises.push(this.handleDiceSoNice());

    // Subtract uses
    const ammoCost = this.action.ammoCost;
    if (ammoCost != 0) premessage_promises.push(this.subtractAmmo(ammoCost));

    if (shared.rollData.chargeCost < 0 || shared.rollData.chargeCost > 0)
      premessage_promises.push(this.item.addCharges(-shared.rollData.chargeCost));
    if (shared.action.isSelfCharged)
      premessage_promises.push(shared.action.update({ "uses.self.value": shared.action.data.uses.self.value - 1 }));

    await Promise.all(premessage_promises);

    // Update remaining ammo for chat message display
    this.updateAmmoUsage();

    // Retrieve message data
    await this.getMessageData();

    // Post message
    let result;
    if (shared.scriptData?.hideChat !== true) {
      result = this.postMessage();
    }

    // Deselect targets
    if (game.settings.get("pf1", "clearTargetsAfterAttack") && game.user.targets.size) {
      game.user.updateTokenTargets([]);
      // Above does not communicate targets to other users, so..
      game.user.broadcastActivity({ targets: [] });
    }

    // Call post-use script calls
    await this.executeScriptCalls("postUse");

    Hooks.callAll("pf1PostActionUse", this, result instanceof pf1.documents.ChatMessagePF ? result : null);

    return result;
  }
}

/**
 * @typedef {object} ItemAttack_Dialog_Result
 * @property {boolean} fullAttack - Whether it's a full attack (true) or a single attack (false)
 * @property {JQuery} html - The html containing user input and selections.
 */
